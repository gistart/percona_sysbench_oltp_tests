sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 4
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            0
        write:                           8649
        other:                           0
        total:                           8649
    transactions:                        8649   (144.09 per sec.)
    queries:                             8649   (144.09 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0218s
    total number of events:              8649

Latency (ms):
         min:                                  7.79
         avg:                                 27.75
         max:                                180.86
         95th percentile:                     31.37
         sum:                             240045.94

Threads fairness:
    events (avg/stddev):           2162.2500/3.11
    execution time (avg/stddev):   60.0115/0.01

