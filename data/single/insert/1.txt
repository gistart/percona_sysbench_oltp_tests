sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 4
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            0
        write:                           8737
        other:                           0
        total:                           8737
    transactions:                        8737   (145.56 per sec.)
    queries:                             8737   (145.56 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0198s
    total number of events:              8737

Latency (ms):
         min:                                  6.72
         avg:                                 27.47
         max:                                182.85
         95th percentile:                     30.81
         sum:                             240035.36

Threads fairness:
    events (avg/stddev):           2184.2500/1.79
    execution time (avg/stddev):   60.0088/0.01

