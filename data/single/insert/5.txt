sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 4
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            0
        write:                           8720
        other:                           0
        total:                           8720
    transactions:                        8720   (145.28 per sec.)
    queries:                             8720   (145.28 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0213s
    total number of events:              8720

Latency (ms):
         min:                                  6.84
         avg:                                 27.53
         max:                                233.46
         95th percentile:                     30.81
         sum:                             240043.30

Threads fairness:
    events (avg/stddev):           2180.0000/4.85
    execution time (avg/stddev):   60.0108/0.01

