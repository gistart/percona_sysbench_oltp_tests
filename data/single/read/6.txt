sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 4
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            691390
        write:                           0
        other:                           0
        total:                           691390
    transactions:                        49385  (822.98 per sec.)
    queries:                             691390 (11521.74 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0058s
    total number of events:              49385

Latency (ms):
         min:                                  3.38
         avg:                                  4.86
         max:                                 14.16
         95th percentile:                      5.47
         sum:                             239962.70

Threads fairness:
    events (avg/stddev):           12346.2500/241.86
    execution time (avg/stddev):   59.9907/0.00

