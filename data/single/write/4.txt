sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 4
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            0
        write:                           8616
        other:                           0
        total:                           8616
    transactions:                        2154   (35.87 per sec.)
    queries:                             8616   (143.46 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0550s
    total number of events:              2154

Latency (ms):
         min:                                 87.06
         avg:                                111.48
         max:                                174.04
         95th percentile:                    121.08
         sum:                             240134.08

Threads fairness:
    events (avg/stddev):           538.5000/0.50
    execution time (avg/stddev):   60.0335/0.02

