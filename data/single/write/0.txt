sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 4
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            0
        write:                           8796
        other:                           0
        total:                           8796
    transactions:                        2199   (36.62 per sec.)
    queries:                             8796   (146.49 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0435s
    total number of events:              2199

Latency (ms):
         min:                                 67.18
         avg:                                109.19
         max:                                256.87
         95th percentile:                    130.13
         sum:                             240119.65

Threads fairness:
    events (avg/stddev):           549.7500/1.09
    execution time (avg/stddev):   60.0299/0.01

