sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 4
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            0
        write:                           8780
        other:                           0
        total:                           8780
    transactions:                        2195   (36.52 per sec.)
    queries:                             8780   (146.10 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0952s
    total number of events:              2195

Latency (ms):
         min:                                 42.85
         avg:                                109.44
         max:                                283.43
         95th percentile:                    130.13
         sum:                             240231.00

Threads fairness:
    events (avg/stddev):           548.7500/2.05
    execution time (avg/stddev):   60.0578/0.03

