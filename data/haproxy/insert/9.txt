sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 8
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            0
        write:                           18332
        other:                           0
        total:                           18332
    transactions:                        18332  (305.43 per sec.)
    queries:                             18332  (305.43 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0188s
    total number of events:              18332

Latency (ms):
         min:                                  4.33
         avg:                                 26.18
         max:                                 64.63
         95th percentile:                     29.72
         sum:                             480016.46

Threads fairness:
    events (avg/stddev):           2291.5000/36.75
    execution time (avg/stddev):   60.0021/0.01

