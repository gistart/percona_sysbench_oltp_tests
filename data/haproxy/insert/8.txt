sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 8
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            0
        write:                           18255
        other:                           0
        total:                           18255
    transactions:                        18255  (304.15 per sec.)
    queries:                             18255  (304.15 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0183s
    total number of events:              18255

Latency (ms):
         min:                                  3.00
         avg:                                 26.29
         max:                                 57.42
         95th percentile:                     29.72
         sum:                             480014.01

Threads fairness:
    events (avg/stddev):           2281.8750/6.39
    execution time (avg/stddev):   60.0018/0.01

