sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 8
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            1036000
        write:                           0
        other:                           0
        total:                           1036000
    transactions:                        74000  (1233.19 per sec.)
    queries:                             1036000 (17264.59 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0057s
    total number of events:              74000

Latency (ms):
         min:                                  4.59
         avg:                                  6.49
         max:                                 15.78
         95th percentile:                      7.43
         sum:                             479934.77

Threads fairness:
    events (avg/stddev):           9250.0000/606.38
    execution time (avg/stddev):   59.9918/0.00

