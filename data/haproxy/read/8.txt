sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 8
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            1037470
        write:                           0
        other:                           0
        total:                           1037470
    transactions:                        74105  (1234.89 per sec.)
    queries:                             1037470 (17288.47 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0079s
    total number of events:              74105

Latency (ms):
         min:                                  4.49
         avg:                                  6.48
         max:                                 13.77
         95th percentile:                      7.43
         sum:                             479944.92

Threads fairness:
    events (avg/stddev):           9263.1250/592.53
    execution time (avg/stddev):   59.9931/0.00

