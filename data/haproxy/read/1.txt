sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 8
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            1043196
        write:                           0
        other:                           0
        total:                           1043196
    transactions:                        74514  (1241.75 per sec.)
    queries:                             1043196 (17384.44 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0060s
    total number of events:              74514

Latency (ms):
         min:                                  4.36
         avg:                                  6.44
         max:                                 14.34
         95th percentile:                      7.43
         sum:                             479938.36

Threads fairness:
    events (avg/stddev):           9314.2500/541.79
    execution time (avg/stddev):   59.9923/0.00

