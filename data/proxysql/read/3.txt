sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 8
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            1269982
        write:                           0
        other:                           0
        total:                           1269982
    transactions:                        90713  (1511.71 per sec.)
    queries:                             1269982 (21163.91 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0055s
    total number of events:              90713

Latency (ms):
         min:                                  3.56
         avg:                                  5.29
         max:                                 14.53
         95th percentile:                      5.88
         sum:                             479942.14

Threads fairness:
    events (avg/stddev):           11339.1250/79.98
    execution time (avg/stddev):   59.9928/0.00

