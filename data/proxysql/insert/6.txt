sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 8
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            0
        write:                           19634
        other:                           0
        total:                           19634
    transactions:                        19634  (327.11 per sec.)
    queries:                             19634  (327.11 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0215s
    total number of events:              19634

Latency (ms):
         min:                                  3.96
         avg:                                 24.45
         max:                                 73.07
         95th percentile:                     29.72
         sum:                             480047.18

Threads fairness:
    events (avg/stddev):           2454.2500/11.19
    execution time (avg/stddev):   60.0059/0.01

