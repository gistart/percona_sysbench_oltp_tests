sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 8
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            0
        write:                           20078
        other:                           0
        total:                           20078
    transactions:                        20078  (334.46 per sec.)
    queries:                             20078  (334.46 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0301s
    total number of events:              20078

Latency (ms):
         min:                                  4.88
         avg:                                 23.91
         max:                                 48.74
         95th percentile:                     29.72
         sum:                             480092.40

Threads fairness:
    events (avg/stddev):           2509.7500/11.21
    execution time (avg/stddev):   60.0116/0.01

