sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 8
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            0
        write:                           20281
        other:                           0
        total:                           20281
    transactions:                        20281  (337.89 per sec.)
    queries:                             20281  (337.89 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0214s
    total number of events:              20281

Latency (ms):
         min:                                  4.35
         avg:                                 23.67
         max:                                 44.28
         95th percentile:                     29.72
         sum:                             479983.46

Threads fairness:
    events (avg/stddev):           2535.1250/10.43
    execution time (avg/stddev):   59.9979/0.01

