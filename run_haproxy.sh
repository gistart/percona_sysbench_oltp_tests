#! /bin/sh

MYP0="--mysql-host=192.168.1.100"
MYP1="--mysql-host=192.168.1.101"

MYPROXY="--mysql-host=192.168.1.50"
PROXYNAME="haproxy"

MYALL="--db-driver=mysql --mysql-user=sbtest --mysql-password=sbtest --mysql-db=sbtest"

COMMONARGS="--threads=8 --time=60"
TESTARGS="--skip_trx=on --tables=1 --table-size=1000000"


#sysbench $COMMONARGS $MYP0 $MYALL /usr/share/sysbench/oltp_read_only.lua  $TESTARGS prepare
#sysbench $COMMONARGS $MYP1 $MYALL /usr/share/sysbench/oltp_read_only.lua  $TESTARGS prepare
#
#for i in 0 1 2 3 4 5 6 7 8 9
#do
#    sysbench $MYPROXY $MYALL $COMMONARGS /usr/share/sysbench/oltp_read_only.lua $TESTARGS run > ./data/$PROXYNAME/read/$i.txt
#done
#
#sysbench $MYP0 $MYALL $COMMONARGS /usr/share/sysbench/oltp_read_only.lua $TESTARGS cleanup
#sysbench $MYP1 $MYALL $COMMONARGS /usr/share/sysbench/oltp_read_only.lua $TESTARGS cleanup
#
#
#
#sysbench $COMMONARGS $MYP0 $MYALL /usr/share/sysbench/oltp_write_only.lua  $TESTARGS prepare
#sysbench $COMMONARGS $MYP1 $MYALL /usr/share/sysbench/oltp_write_only.lua  $TESTARGS prepare
#
#for i in 0 1 2 3 4 5 6 7 8 9
#do
#    sysbench $MYPROXY $MYALL $COMMONARGS /usr/share/sysbench/oltp_write_only.lua $TESTARGS run > ./data/$PROXYNAME/write/$i.txt
#done
#
#sysbench $MYP0 $MYALL $COMMONARGS /usr/share/sysbench/oltp_write_only.lua $TESTARGS cleanup
#sysbench $MYP1 $MYALL $COMMONARGS /usr/share/sysbench/oltp_write_only.lua $TESTARGS cleanup


sysbench $COMMONARGS $MYP0 $MYALL /usr/share/sysbench/oltp_insert.lua  $TESTARGS prepare
sysbench $COMMONARGS $MYP1 $MYALL /usr/share/sysbench/oltp_insert.lua  $TESTARGS prepare

for i in 0 1 2 3 4 5 6 7 8 9
do
    sysbench $MYPROXY $MYALL $COMMONARGS /usr/share/sysbench/oltp_insert.lua $TESTARGS run > ./data/$PROXYNAME/insert/$i.txt
done

sysbench $MYP0 $MYALL $COMMONARGS /usr/share/sysbench/oltp_insert.lua $TESTARGS cleanup
sysbench $MYP1 $MYALL $COMMONARGS /usr/share/sysbench/oltp_insert.lua $TESTARGS cleanup
